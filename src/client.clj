(ns client
  (:refer-clojure :exclude [read])
  (:require
   [clojure.java.io]
   [clojure.string]))


;; with Clojure's *in* and *out* you can write strings directly to stdin and out
;; (they are Java reader/writer; char streams) but since i'm trying to port the
;; Deno version, i'm going to use Java InputStream and OutputStream classes,
;; which operate on byte arrays alone

(def stdin (clojure.java.io/input-stream System/in))

(def stdout (clojure.java.io/output-stream System/out))

(def end "END")


(defn ^String decode [^bytes array]
  (String. array))


(defn ^"[B" encode [^String string]
  (.getBytes string))


(defn error [^String msg]
  (throw (Error. msg)))


(defn write
  ([msg]
   (write msg stdout))
  ([msg ^java.io.OutputStream output]
   (.write output (encode msg))
   (.flush output)))


(defn read [& {:keys [^java.io.InputStream input prompt output]
               :or {input stdin output stdout}}]
  (if prompt
    (write prompt output))
  (let [buff (byte-array 128)
        byte-count (.read input buff)]
    (if (= byte-count -1)
      end
      (clojure.string/trim
       (decode
        (java.util.Arrays/copyOfRange buff 0 byte-count))))))


(defn connect [^String hostname ^Integer port]
  (with-open [sock (java.net.Socket. hostname port)
              sock-output-stream (clojure.java.io/output-stream sock)
              sock-input-stream (clojure.java.io/input-stream sock)]
    (println "Connected to" (str hostname \: port \,) "type END to finish")
    (let [username (read :prompt "Type your username: ")
          msg "OK"]
      (println "Send the message" end "to finish")
      (loop []
        (let [msg (read :prompt "Write a message and type enter to send it\n")]
          (if-not (= msg end)
            (do (write (str \[ username "]: " msg \newline)
                       sock-output-stream)
                (println (read :input sock-input-stream))
                (recur)))))))
  (println "Connection closed"))


(defn -main [& args]
  (if (< (count args) 2)
    (error "Missing arguments.\nArgs: [HOST] [PORT]"))
  (let [[host port] args]
    (try
      (let [port (Integer/parseInt port)]
        (connect host port))
      (catch NumberFormatException err
        (error (str "Failed parsing port \"" port "\""))))))
