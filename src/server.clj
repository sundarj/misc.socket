(ns server
  (:require [clojure.java.io]))


(defn ^String decode [^bytes array]
  (String. array))


(defn ^"[B" encode [^String string]
  (.getBytes string))


(def end "END")


(defn handle-connection [^java.net.Socket conn]
  (let [buff (byte-array 1024)
        hostname (.. conn getInetAddress getHostName)
        port (.getPort conn)]
    (with-open [conn conn
                conn-input-stream (clojure.java.io/input-stream conn)
                conn-output-stream (clojure.java.io/output-stream conn)]
      (try
        (let [msg "OK"]
          (loop []
            (let [byte-count (.read conn-input-stream buff)
                  msg (clojure.string/trim
                       (decode
                        (java.util.Arrays/copyOfRange buff 0 byte-count)))]
              (if-not (= msg end)
                (do (println msg)
                    (.write conn-output-stream (encode "RECEIVED"))
                    (.flush conn-output-stream)
                    (recur))))))
        (catch Exception err
          (if (= (.getMessage err) "Broken pipe")
            (println "Client" (str hostname \: port) "disconnected unexpectedly")
            (println err)))))
    (println "Connection with" (str hostname \: port) "closed")))


(defn listen [port]
  (println "Server running on port" port)
  (with-open [server-sock (java.net.ServerSocket. port)]
    (loop [conn (.accept server-sock)]
      (let [hostname (.. conn getInetAddress getHostName)
            port (.getPort conn)]
        (println "New connection from" (str hostname \: port))
        (future (deref (future (handle-connection conn))))
        (recur (.accept server-sock))))))


(defn error [^String msg]
  (throw (Error. msg)))


(defn -main [& args]
  (if (< (count args) 1)
    (error "No port number received"))
  (let [[port] args]
    (try
      (listen (Integer/parseInt port))
      (catch NumberFormatException err
        (error (str "Failed parsing port " \' port \')))
      (catch Exception err
        (println err)
        (error "Server crashed")))))
